from django.shortcuts import render
from lab_1.models import Friend
from django.http import HttpResponseRedirect
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)
    friends = Friend.objects.all()
    response = {'friends': friends}  
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return render(request, 'lab3_index.html', response)
    else:
        return render(request, 'lab3_form.html', {'form': form})