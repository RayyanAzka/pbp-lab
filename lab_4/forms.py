from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
	class Meta:
		model = Note
		fields = "__all__"
		widget = {
			'To': forms.TextInput(attrs={'class': 'form-control'}),
			'From': forms.TextInput(attrs={'class': 'form-control'}),
			'Title': forms.TextInput(attrs={'class': 'form-control'}),
			'Message': forms.TextInput(attrs={'class': 'form-control'})
		}
